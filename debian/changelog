q2-phylogeny (2024.5.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Regenerate debian/control from debian/control.in (routine-update)
  * d/patches/configparser.patch: copy from qiime
  * Only build for Python 3.11 until upstream catches up with Python
    3.12.

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 26 Jun 2024 13:22:45 +0200

q2-phylogeny (2024.2.0-1) unstable; urgency=medium

  * New upstream version
  * Autopkgtest for all supported Python3 versions
  * Regenerate debian/control from debian/control.in (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 18 Feb 2024 15:21:19 +0100

q2-phylogeny (2023.9.0-1) unstable; urgency=medium

  * New upstream version
  * Generate debian/control automatically to refresh version number
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Regenerate debian/control from debian/control.in (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 30 Jan 2024 19:43:12 +0100

q2-phylogeny (2023.7.0-1) unstable; urgency=medium

  * New upstream version 2023.7.0
  * skip-test-needing-internet.patch: new.
  * iqtree2.patch: refresh.
  * d/copyright: fix copyright year.
  * d/control: add myself to uploaders.

 -- Étienne Mollier <emollier@debian.org>  Sun, 20 Aug 2023 16:36:53 +0200

q2-phylogeny (2022.11.1-3) unstable; urgency=medium

  * Restrict architectures to any-amd64
    Closes: #1029916

 -- Andreas Tille <tille@debian.org>  Wed, 01 Feb 2023 15:25:31 +0100

q2-phylogeny (2022.11.1-2) unstable; urgency=medium

  * Upload to unstable
  * Restrict architectures to any-amd64 any-i386

 -- Andreas Tille <tille@debian.org>  Tue, 24 Jan 2023 18:52:02 +0100

q2-phylogeny (2022.11.1-1) experimental; urgency=medium

  * New upstream version
  * Bump versioned Depends

 -- Andreas Tille <tille@debian.org>  Sat, 14 Jan 2023 16:39:48 +0100

q2-phylogeny (2022.8.0-3) unstable; urgency=high

  * Team Upload.
  * Change arch to all, as this installs arch-indep files
  * Bump Standards-Version to 4.6.2 (no changes needed)

 -- Nilesh Patra <nilesh@debian.org>  Wed, 04 Jan 2023 20:21:31 +0530

q2-phylogeny (2022.8.0-2) unstable; urgency=medium

  * Build-Depends: iqtree + Architecture: any
    This will prevent the attempt to run debci on this package for those
    architectures where iqtree is not available (all except amd64 and i386)

 -- Andreas Tille <tille@debian.org>  Fri, 30 Sep 2022 08:24:29 +0200

q2-phylogeny (2022.8.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2022.8.0
  * Refresh patch
  * Update dependency on qiime/q2-* >= 2022.8.0

 -- Mohammed Bilal <mdbilal@disroot.org>  Wed, 07 Sep 2022 11:14:28 +0000

q2-phylogeny (2022.2.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Add versioned Depends from q2-* packages
  * iqtree in Debian is iqtree version 2 and the binary is named accordingly
  * Fix spelling of fasttreeMP

 -- Andreas Tille <tille@debian.org>  Wed, 20 Jul 2022 16:13:28 +0200

q2-phylogeny (2021.4.0-2) unstable; urgency=medium

  * Team upload.
    + Source-only upload for testing migration

 -- Nilesh Patra <nilesh@debian.org>  Sat, 06 Nov 2021 19:52:48 +0530

q2-phylogeny (2021.4.0-1) unstable; urgency=medium

  * Initial release (Closes: #991240)

 -- Andreas Tille <tille@debian.org>  Sun, 18 Jul 2021 18:56:23 +0530
